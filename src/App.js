import React from 'react';
import './App.css';

const API_ENDPOINT = "https://tl6wrtavwf.execute-api.us-west-2.amazonaws.com/produccion1"

class App extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      id: '',
      saldo_sumar: '',
      list_users: [
        { id: 'f8791d40-e0ff-11e9-af66-bf5f2e142c2e',  nombre: 'Andres Vargas'},
        { id: 'f8791d40-e0ff-11e9-af66-bf5f2e142c22',  nombre: 'Pedro Perez'}
      ]
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)



    fetch(API_ENDPOINT + '/listadeusuario')
    .then(res => res.json())
    .then(data => {
      this.setState({list_users: data.Items})
    })
  }

  handleChange(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name



    this.setState({[name]: value});
  }

  handleSubmit(event){
    event.preventDefault();

    if(this.state.id.length <= 0)
      return;

    let data = {
      id: this.state.id,
      saldo_sumar: this.state.saldo_sumar
    }

    fetch(API_ENDPOINT + '/sumarsaldo' , {
      method: 'POST',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      }

    })
    .then(res => res.json())
    .then(data => {
      alert("El nuevo saldo del usuario es " + data);
      this.refs.form.reset()
      this.setState({
        saldo_sumar: "",
        id: ""
      })
    })
  }

  render(){
    return (
      <div className="App">
        <form id="mainForm" className="App-form" onSubmit={this.handleSubmit} ref="form">
          <strong>Recarga Transmilenio</strong>
          <label>
            Ingrese ID de la tarjeta:
            <select name="id" onChange={this.handleChange} value={this.state.id} >
            <option value="">Seleccione un usuario</option>
            {
              this.state.list_users.map(user =>  <option value={user.id} key={user.id}>{user.Nombre}</option>)
            }
            </select>  
          </label>
          <label>
            Ingrese el monto a recargar:
            <input type="text" name="saldo_sumar" value={this.state.saldo_sumar} onChange={this.handleChange}/> 
          </label>

          <input type="submit" value="REALIZAR RECARGA" />
        </form>
      </div>
      )
  }

}


export default App;